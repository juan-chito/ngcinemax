import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeeklyBillboardComponent } from './weekly-billboard.component';

describe('WeeklyBillboardComponent', () => {
  let component: WeeklyBillboardComponent;
  let fixture: ComponentFixture<WeeklyBillboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeeklyBillboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeeklyBillboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
