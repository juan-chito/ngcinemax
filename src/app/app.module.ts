import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MoviesFilterComponent } from './movies-filter/movies-filter.component';
import { SlidesComponent } from './slides/slides.component';
import { WeeklyBillboardComponent } from './weekly-billboard/weekly-billboard.component';
import { NextReleaseComponent } from './next-release/next-release.component';
import { CarouselComponent } from './carousel/carousel.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MoviesFilterComponent,
    SlidesComponent,
    WeeklyBillboardComponent,
    NextReleaseComponent,
    CarouselComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
